module.exports = {
  keys: {
    // {char, code, codepoint}
    // see https://github.com/cronvel/terminal-kit/blob/HEAD/doc/events.md#ref.event.key
    reopen: {char: "LEFT"},
    discard: {char: "RIGHT"},
    players: [
      {char: "q"},
      {char: "o"},
      {char: "z"},
      {char: "m"},
    ],
  },
  chalk: {
    default: [],
    available: ["bold"],
    discarded: ["gray"],
    players: [
      ["bold", "magenta"],
      ["bold", "red"],
      ["bold", "green"],
      ["bold", "yellow"],
    ],
  },
  countdown: { // set to null if no countdown should be played
    file: "static/countdown.ogg",
    cmd: "paplay",
  },
  applications: [ // any file ending associations...
    {
      ext: ["txt"],
      cmd: "l3afpad",
      kill: "SIGINT",
    },
    {
      ext: ["png", "gif", "jpeg", "jpg"],
      cmd: "imv", // recommended image previewer: https://github.com/eXeC64/imv
    },
    {
      ext: ["svg"],
      cmd: "xsvg",
      argv: ["-f", "%F"],
    },
    {
      countdown: false,
      ext: ["webm", "mkv"],
      cmd: "cvlc",
      argv: ["-R", "--no-video-title-show", "%F"], // "%F" within arguments is replaced file name
    },
    {
      countdown: false,
      ext: ["ogg", "mp3"],
      cmd: "paplay",
      argv: ["%F"], // default arguments
    },
  ],
};
