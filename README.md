# Node.js Jeopardy

A terminal-based application for [Jeopardy!](https://www.jeopardy.com/)-like quiz shows.

![Screenshot](https://gitlab.com/frissdiegurke/jeopardy/raw/master/static/screenshot.png)

## Features

* Intuitive and simple menu
* Any category and score combinations
* Custom bonus/penalty points
* External file preview via `xdg-open` or as configured
* Immediately persisted game status
* Special fields (player selects value)

## How it works

The node.js application is started within a terminal. It has a interactive mode for category/score selection and candidate input.
When a category and score is selected, the application starts a new process (using `xdg-open` or any application specified within config file) for the respective file.
Thus in order to work properly the window manager needs to keep the focus on the terminal instead of focusing the newly opened window.

## Setup

### Window manager

The recommended option is to use a window manager like [herbstluftwm](http://herbstluftwm.org/) with a side-by-side layout and configured to not focus new clients:

```bash
herbstclient load 1 '(clients horizontal:0.5)'
herbstclient rule focus=off
```

### Mime-types

File associations are set-up within *config.js* (`applications`). You may set-up commands on a per-field basis within *data/game.js* as well (see below).

### Hardware

For simple events players may use plain keyboards and have each a key assigned. More ideal would be buzzers that transmit a single key-press for the assigned key. Any big-enough display or projector suffices (recommended is Full-HD but it depends on screen-size and amount of categories/scores per game).

### Dependencies

This application needs `npm` and `node.js` to be installed. Further `xdg-open` is used for fallback file execution.

Run `npm install` to prepare dependencies.

You may specify a countdown file that is executed after field selection (if the field command does not specify `countdown:false`) within *config.js* (`countdown`). For personal use, I can recommend the [jeopardy theme song](http://www.orangefreesounds.com/jeopardy-theme-song/).

### Run

Use `npm start` within package directory to start the game (as defined below).

### Configuration

Change the *config.js* file as needed

## Game data

The game data needs to be placed within a *data/* directory/symlink to be created within the project root.

Define the game within *data/game.js* following this schema:

```javascript
module.exports = {
  players: ["Jon Doe", "Peter", "Alice", "Bob", /*...*/],
  scores: [100, 200, 300, 400, 500, /*...*/],
  categories: [ // {name, prefix = name, scores = ../scores}
    {
      name: "Cats",
      prefix: "c", // find file like data/files/c.INDEX.EXT 
                   // (INDEX is the score index beginning with 0, EXT is any file extension)
    },
    {
      name: "Quotes",
      prefix: "q",
      scores: [
        // number|null|{value: number|{display, min, max}, file, countdown, cmd, argv, kill}
        100, // simple number value
        200,
        null, // no such field for this category
        { // use a custom command for quotes/400 (paplay within loop)
          value: 400,
          cmd: "bash",
          argv: ["-c", "while true; do paplay '%F'; sleep 1; done"],
          kill: "SIGTERM",
          // you may specify `file` as well (relative to data/files/)
        },
        { // make value of quotes/500 a range from 0 to 1000 (to be chosen by selecting player)
          value: {display: 500, min: 0, max: 1000},
        },
        // ...
      ],
    },
    // ...
  ],
};
```

Create exactly one file for **every** score/category combination within *data/files/* like *data/files/c.0.txt* (any file ending allowed, 0-based index of score).

Ensure within *config.js* at least as many player keys (and chalks) are specified as used within *data/game.js*. Any chalk value that is the name of a chain-able function (with no required parameters) [available within `terminal-kit`](https://github.com/cronvel/terminal-kit/blob/HEAD/doc/low-level.md#ref.colors) is available for output formatting.

## Misc

During waiting time for player selection:

* Use the reopen key (default: left arrow key) to re-open a file (e.g. re-play video).
* Use the discard key (default: right arrow key) to stop during the player selection time and discard the current field.

## License

This Software is licensed under the [GPL-3.0+](https://gitlab.com/frissdiegurke/jeopardy/raw/master/LICENSE).
