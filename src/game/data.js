import path from "path";

import {getFallback} from "./status";
import {readJSON, writeJSON} from "../io/file";
import game from "../../data/game";

const BASE_DIR = path.join(__dirname, "..", "..", "data");
const STATUS_FILE = path.join(BASE_DIR, "status.json");

/*===================================================== Exports  =====================================================*/

export {read, persist};

/*==================================================== Functions  ====================================================*/

async function persist({status}) { await writeJSON(STATUS_FILE, status); }

async function read() {
  const status = await readJSON(STATUS_FILE, getFallback);
  return {status, game};
}
