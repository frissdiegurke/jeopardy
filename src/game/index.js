/* eslint-disable no-constant-condition */
import path from "path";
import {find, some, assign} from "lodash";
import {terminal} from "terminal-kit";

import {print as printData, printPlayerName} from "../io/output";
import {findFile} from "../cmd/files";
import {countdown, spawn} from "../cmd/spawn";
import {fieldSelect, firstPlayerChar, getScoreValue, isCorrect} from "../io/input";
import {persist as persistData} from "./data";
import {defChalk} from "../io/chalk";
import config from "../../config";
import {discardField, getAvailableFields, invalidatePlayer, resolveField, setFieldValue} from "./status";
import {matcher} from "../io/keys";

const BASE_DIR = path.join(__dirname, "..", "..", "data", "files");

/*===================================================== Exports  =====================================================*/

export {run};

/*==================================================== Functions  ====================================================*/

async function run(data) {
  const persist = persistData.bind(null, data);
  const print = printData.bind(null, data);
  // game loop
  while (true) {
    print();
    const availableFields = getAvailableFields(data);
    if (!availableFields.length) {
      terminal.green("\nGame resolved.");
      break;
    }
    defChalk("\n");
    printPlayerName(data, data.status.active);
    defChalk(", choose a field:");
    // todo skip some inputs if some field was in progress within status file
    // select field
    const {categoryIndex, scoreIndex} = await fieldSelect(data, availableFields);
    const category = data.game.categories[categoryIndex];
    const scoreValue = await getScoreValue(data, categoryIndex, scoreIndex);
    setFieldValue(data, categoryIndex, scoreIndex, scoreValue);
    persist();
    // collect field data
    const file = await getFile(data, category, scoreIndex);
    const cmdData = assign({}, getCmdOptions(data, category, scoreIndex, file), {file});
    // spawn external process
    const process = spawn(cmdData);
    const stopReopenListener = attachReopenListener(process);
    const bgSound = cmdData.countdown === false ? null : countdown();
    // field buzzer loop
    while (true) {
      const playerIndex = await firstPlayerChar(data);
      if (playerIndex == null) { // discard key has been pressed
        discardField(data, categoryIndex, scoreIndex);
        persist();
        break;
      }
      printPlayerName(data, playerIndex);
      defChalk("\n");
      // wait for jury decision
      const result = await isCorrect(scoreValue);
      // handle jury decision
      if (!result.nullify) {
        if (result.valid) {
          setFieldValue(data, categoryIndex, scoreIndex, result.value);
          resolveField(data, categoryIndex, scoreIndex, playerIndex);
          persist();
          break;
        }
        invalidatePlayer(data, categoryIndex, scoreIndex, playerIndex, result.value);
        persist();
      }
      print();
    }
    // destruct external processes
    stopReopenListener();
    await process.stop();
    if (bgSound != null) { bgSound.stop(); }
  }
}

function attachReopenListener(process) {
  const reopenMatcher = matcher(config.keys.reopen, () => process.restart());
  terminal.on("key", reopenMatcher);
  return () => terminal.off("key", reopenMatcher);
}

async function getFile({game}, category, scoreIndex) {
  const scoreData = (category.scores || game.scores)[scoreIndex];
  if (scoreData.hasOwnProperty("file")) { return path.join(BASE_DIR, scoreData.file); }
  return await findFile(BASE_DIR, `${category.prefix || category.name}.${scoreIndex}`);
}

function getCmdOptions({game}, category, scoreIndex, file) {
  const scoreData = (category.scores || game.scores)[scoreIndex];
  const extOptions = find(config.applications, (app) => some(app.ext, (ext) => file.endsWith("." + ext)));
  if (scoreData.hasOwnProperty("cmd")) { return assign({}, extOptions,scoreData); }
  return assign({}, extOptions);
}
