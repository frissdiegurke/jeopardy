import fs from "fs";
import path from "path";

/*===================================================== Exports  =====================================================*/

export {findFile};

/*==================================================== Functions  ====================================================*/

async function findFile(dir, name) {
  return new Promise((resolve, reject) => {
    fs.readdir(dir, (err, files) => {
      if (err != null) { return reject(err); }
      for (let i = 0; i < files.length; i++) {
        if (files[i].startsWith(name)) { return resolve(path.join(dir, files[i])); }
      }
      reject("File not found.");
    });
  });
}
