import psTree from "ps-tree";

/*===================================================== Exports  =====================================================*/

export {kill};

/*==================================================== Functions  ====================================================*/

async function kill(pid, signal) {
  signal = signal || "SIGINT";
  return await new Promise((resolve, reject) => {
    psTree(pid, (err, children) => {
      if (err != null) { return reject(err); }
      [pid]
        .concat(children.map((p) => p.PID))
        // eslint-disable-next-line no-empty
        .forEach((tpid) => { try { process.kill(tpid, signal); } catch (ignored) { } });
      resolve();
    });
  });
}
