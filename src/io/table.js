import {center} from "../util/pad";

/*===================================================== Exports  =====================================================*/

export {print};

/*==================================================== Functions  ====================================================*/

function print(borderPencil, colWidth, contentSeparator, scopeSeparator, header, rows) {
  // print header
  printRow(borderPencil, colWidth, header);
  borderPencil(scopeSeparator);
  // print body
  for (let rowIdx = 0; rowIdx < rows.length; rowIdx++) {
    if (rowIdx !== 0) { borderPencil(contentSeparator); }
    printRow(borderPencil, colWidth, rows[rowIdx]);
  }
  borderPencil(scopeSeparator);
}

function printRow(borderPencil, colWidth, row) {
  let _lines = row[0].lines.length;
  for (let lineIdx = 0; lineIdx < _lines; lineIdx++) {
    for (let fieldIdx = 0; fieldIdx < row.length; fieldIdx++) {
      if (fieldIdx !== 0) { borderPencil("|"); }
      row[fieldIdx].pencil(center(row[fieldIdx].lines[lineIdx], colWidth + 2));
    }
    borderPencil("\n");
  }
}
