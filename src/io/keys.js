/*===================================================== Exports  =====================================================*/

export {matches, matcher};

/*==================================================== Functions  ====================================================*/

function matcher(key, cb) {
  return (a, b, c) => { if (matches(key, a, b, c)) { cb(); } };
}

function matches(key, name, ignored, {codepoint, code}) {
  if (key.char != null) { return key.char === name; }
  if (key.code != null) { return key.code === code; }
  if (key.codepoint != null) { return key.codepoint === codepoint; }
  return false;
}
