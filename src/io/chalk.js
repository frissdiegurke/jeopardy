import {terminal} from "terminal-kit";

import config from "../../config";

const defChalk = apply(terminal, config.chalk.default);

/*===================================================== Exports  =====================================================*/

export {defChalk};
export default function apply(chalk) {
  if (chalk == null) { return defChalk; }
  let term = terminal;
  for (let i = 0; i < chalk.length; i++) { if (typeof term[chalk[i]] === "function") { term = term[chalk[i]]; } }
  return term;
}
