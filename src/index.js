import {terminal} from "terminal-kit";

import {read} from "./game/data";
import {run} from "./game";
import {shutdown} from "./cmd/spawn";

/*================================================ Initial execution  ================================================*/

start().catch((err) => terminal.error.red(err.stack));

terminal.on("key", (name) => { if (name === "CTRL_C") { terminate(130); } });

/*==================================================== Functions  ====================================================*/

async function start() {
  await run(await read());
  terminate(0);
}

async function terminate(code) {
  await shutdown();
  terminal.processExit(code);
}
